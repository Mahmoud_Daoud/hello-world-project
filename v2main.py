import mysql.connector
from mysql.connector import errorcode
import sys
from sys import stdin

#db = "Article_Stocktaking"
tables=[]

if ((len(sys.argv)!=3) and ((sys.argv[2] != '-test') or (sys.argv[2] != '-config'))):


    #print("You should enter '-test' to update Production Test Results or '-config' to update Configration Parameter")
    print('Usage: The available flags and arguments')
    print("1) database.py filename -test")
    print("2) database.py filename -config")
    exit(1)
#convert to binary
def convertToBinaryData(filename):
    with open(filename, 'rb') as file:
        binaryData = file.read()
    return binaryData

def write_file(data, filename):
    # Convert binary data to proper format and write it on Hard Disk
    with open(filename, 'wb') as file:
        file.write(data)
    
def ShowallTables(cursor,db):   
    try:
        cursor.execute("USE {}".format(db))
        print('The database is used')
        print('The available table are :')
        cursor.execute("SHOW TABLES")    
        #tables = cursor.fetchall() 
        for (table,) in cursor:
            print(table)
            tables.append(table)
            
        for tab in tables:
            print("******{}*******".format(tab))
            cursor.execute("SELECT * FROM {}".format(tab))

            rows = cursor.fetchall()

            for row in rows:
                print(row)
            print("*********************")


    except mysql.connector.Error as err:
        print("Database {} does not exists.".format(db))
        exit(5)

def RetriveDataFromDatabase(cursor,db,table,column,id):
    try:
        cursor.execute("USE {}".format(db))
        print('The database is used')
        cursor.execute("""SELECT {} from {} WHERE ID = {}""".format(column,table,id))
        
        #with open(sys.argv[1],'wb')as file:
        rows = cursor.fetchall()
    
        for row in rows:
            data= row[0]
            write_file(data, sys.argv[1])
        print("*********************")
        
    except mysql.connector.Error as err:
        print("Database {} does not exists.".format(db))
 
        exit(5) 
        
            
            
def InsertIntoProductionTestResults(cursor):
    add_test_result =("INSERT INTO ProductionTestResults" "(ArtNo,SerNo,TestName, TestData)"  "VALUES (%s,%s,%s,%s)")
    file_binary = convertToBinaryData(sys.argv[1])
    if sys.argv[2] == '-test':
        values_test =(1234567896,987654329,'sensor6',file_binary)
        cursor.execute(add_test_result,values_test)
        print("It's done!")

def InsertIntoProductionParameters(cursor):
    
    add_parameter =("INSERT INTO ProductionParameters" "(ArtNo,SerNo,ParameterName, ParameterData)" "VALUES (%s,%s,%s,%s)")    
    file_binary =convertToBinaryData(sys.argv[1])
    #input("enter the name file")
    if sys.argv[1] == '-config':
        values_test =(1234567896,987654322,'sensor5',file_binary)
        cursor.execute(add_parameter,values_test)
        print("It's done!")
       
def main():       
    #connect to database
    try:
        db_connection = mysql.connector.connect(
        host = "localhost",
        user = "root",
        passwd = "Kapsch00",
        db = "Article_Stocktaking"
        #port="3306"
        )
        print('You connect to the database')
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print('Something is wrong with username or password')
            exit(2)
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print('The database is not exits')
            exit(3)
        else:
            print(err)
            exit(4)
    cursor = db_connection.cursor()
   
    #ShowallTables(cursor,"Article_Stocktaking")
    #InsertIntoProductionTestResults(cursor)
    #InsertIntoProductionParameters(cursor)
    RetriveDataFromDatabase(cursor,db='Article_Stocktaking',table='ProductionParameters',column='ParameterData',id=16)
    db_connection.commit()
    cursor.close()
    db_connection.close()
 
if __name__ == "__main__":
    # execute only if run as a script
    main()






